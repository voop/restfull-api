<?php

use Symfony\Component\HttpFoundation\Request;

define('BASE_DIR', realpath(dirname(__FILE__). '/..'));
define('SRC_DIR', sprintf('%s/src', BASE_DIR));

$loader = require BASE_DIR .'/vendor/autoload.php';
$loader->register();

$di = new \League\Container\Container();
// загнать базовые вещи в контейнер DI
(new \Voop\Restfull\Api\Container\DiContainer())->handle($di);

$request = (new \Voop\Restfull\Api\Core\RequestPrepare())->handle();
// загнать в контейнер DI
$di->add(Request::class, $request);
$app = new \Voop\Restfull\Api\Core\Core($di, $di->get(\Voop\Restfull\Api\Core\Config::class));

/** @var Symfony\Component\HttpFoundation\Response $response */
$response = $app->handle($di->get(Request::class));
$response->send();
