Сырой тестовый шаблон-заготовка restfull-АПИ без фремворка.


Создать список товаров:
```
 curl -X PUT http://api.site/api/v1/productlist
```

Получить список товаров:
```
 curl -X PUT http://api.site/api/v1/productlist
```

Создать заказ (со списком товаров):
```
    curl -X PUT   http://api.site/api/v1/order   -H 'Content-Type: application/json'   -H 'cache-control: no-cache'   -d '{
    "product_ids": [
        "18f422d7-09b5-49ce-a596-7297e2466b6c",
        "c902169a-f7b5-4253-9b93-488be65c00b8",
        "d779ceed-f8b0-40b1-90ce-e1897f6d95a3"
    ]}'
```

Оплата заказа:
```
    curl -X POST http://api.site/api/v1/orderpay   -H 'Content-Type: application/json'   -H 'cache-control: no-cache' -d '
    {"order_id": "d779ceed-f8b0-40b1-90ce-e1897f6d95a3", "sum": 123}'
```
