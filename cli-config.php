<?php

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Tools\Setup;

require __DIR__ . '/vendor/autoload.php';

$config = require __DIR__ . '/src/Config/database.php';

$annotationConfiguration = Setup::createAnnotationMetadataConfiguration(
    $config['doctrine']['entity_paths'],
    $config['doctrine']['dev_mode'],
    null,
    null,
    false
);

$entityManager = EntityManager::create(
    $config['db'],
    $annotationConfiguration
);

return ConsoleRunner::createHelperSet($entityManager);
