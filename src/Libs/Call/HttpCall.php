<?php namespace Voop\Restfull\Api\Libs\Call;

use Curl\Curl;
use Voop\Restfull\Api\Providers\Interfaces\ProviderInterface;

/**
 * Class YaCall
 *
 * @package Voop\Restfull\Api\Service\Ya
 */
class HttpCall implements CallInterface
{
    /**
     * @var \Curl\Curl
     */
    private $curl;

    /**
     * @var ResponceInterface
     */
    private $output;

    /**
     * @var string
     */
    private $protocol = 'https://';


    /**
     * @var string
     */
    private $result;

    /**
     * @param \Curl\Curl                                     $curl
     * @param \Voop\Restfull\Api\Libs\Call\ResponceInterface $output
     */
    public function __construct(Curl $curl, ResponceInterface $output)
    {
        $this->curl = $curl;
        $this->output = $output;
    }

    /**
     * Отправка запроса
     *
     * @param ProviderInterface $provider
     * @return CallInterface
     */
    public function call(ProviderInterface $provider) :CallInterface
    {
        $source = $provider->getSource();
        $this->result = $this->curl->get($this->protocol . $source);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getResult() :ResponceInterface
    {
        return $this->output->setStatus($this->curl->getHttpStatusCode());
    }
}
