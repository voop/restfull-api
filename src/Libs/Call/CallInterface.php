<?php namespace Voop\Restfull\Api\Libs\Call;

use Voop\Restfull\Api\Providers\Interfaces\ProviderInterface;

/**
 * Interface CallInterface
 *
 * @package Voop\Restfull\Api\Libs\Call
 */
interface CallInterface
{
    /**
     * @param \Voop\Restfull\Api\Providers\Interfaces\ProviderInterface $provider
     * @return mixed
     */
    public function call(ProviderInterface $provider) :CallInterface;


    /**
     * @return \Voop\Restfull\Api\Libs\Call\ResponceInterface
     */
    public function getResult() :ResponceInterface;
}
