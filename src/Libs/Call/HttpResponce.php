<?php namespace Voop\Restfull\Api\Libs\Call;

/**
 * Class HttpResponce
 *
 * @package Voop\Restfull\Api\Libs\Call
 */
class HttpResponce implements ResponceInterface
{
    /**
     * @var
     */
    private $status;

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return \Voop\Restfull\Api\Libs\Call\ResponceInterface
     */
    public function setStatus($status) :ResponceInterface
    {
        $this->status = $status;
        return $this;
    }


    /**
     * @return bool
     */
    public function isSuccess() :bool
    {
        return $this->getStatus() == 200;
    }
}
