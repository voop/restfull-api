<?php namespace Voop\Restfull\Api\Libs\Call;

interface ResponceInterface
{
    /**
     * @return mixed
     */
    public function getStatus();

    /**
     * @param mixed $status
     */
    public function setStatus($status);

    /**
     * @return bool
     */
    public function isSuccess() :bool;
}
