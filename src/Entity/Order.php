<?php namespace Voop\Restfull\Api\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use LogicException;

/**
 * @ORM\Entity(repositoryClass="Voop\Restfull\Api\Repository\OrderRepository")
 * @ORM\Table(name="orders")
 */
class Order
{
    /**
     * @var string
     */
    public const STATUS_NEW = 'new';

    /**
     * @var string
     */
    public const STATUS_PAID = 'paid';

    /**
     * @var string
     *
     * @ORM\Id()
     * @ORM\Column(type="string")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $status;

    /**
     * @var Product[]
     *
     * @ORM\ManyToMany(targetEntity="Product")
     * @ORM\JoinTable(name="products_orders",
     *      joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="order_id", referencedColumnName="id")}
     *      )
     */
    private $products;

    /**
     * @param string $id
     * @param string $status
     * @param Product[] $products
     */
    public function __construct(string $id, string $status, array $products)
    {
        $this->id = $id;
        $this->status = $status;
        $this->products = new ArrayCollection($products);

        $this->checkIfStatusCorrect($status);
        $this->amount = $this->calcTotal();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return Product[]
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @return bool
     */
    public function isNew(): bool
    {
        return $this->status === self::STATUS_NEW;
    }

    public function setPaidStatus(): void
    {
        $this->status = self::STATUS_PAID;
    }

    /**
     * @param string $status
     *
     * @throws LogicException
     */
    private function checkIfStatusCorrect(string $status): void
    {
        if (!in_array($status, [self::STATUS_NEW, self::STATUS_PAID], true)) {
            throw new LogicException("Incorrect status {$status} for order");
        }
    }

    /**
     * @return int
     */
    private function calcTotal(): int
    {
        $prices = $this->products->map(function (Product $product) {
            return $product->getPrice();
        });

        return (int) array_sum($prices->toArray());
    }
}
