<?php namespace Voop\Restfull\Api\Entity;

/**
 * @ORM\Entity(repositoryClass="Voop\Restfull\Api\Repository\ProductRepository")
 * @ORM\Table(name="products")
 */
class Product
{
    /**
     * @var string
     *
     * @ORM\Id()
     * @ORM\Column(type="string")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $title;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private $price;

    /**
     * @param string $id
     * @param string $title
     * @param int    $price
     */
    public function __construct(string $id, string $title, int $price)
    {
        $this->id = $id;
        $this->title = $title;
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }
}
