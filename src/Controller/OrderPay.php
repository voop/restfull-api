<?php namespace Voop\Restfull\Api\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Voop\Restfull\Api\Core\UrlParts;
use Voop\Restfull\Api\Service\OrderPayService;


class OrderPay implements ControllerInterface
{
    /**
     * @var \Voop\Restfull\Api\Service\OrderCreateService
     */
    private $service;



    private $request;



    public function __construct(Request $request, OrderPayService $service)
    {
        $this->service = $service;
        $this->request = $request;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function handle() :JsonResponse
    {
        /** @var \Voop\Restfull\Api\Entity\Order $order */
        $order = $this->service->handle($this->request->get('order_id'), (int)$this->request->get('sum'));

        return new JsonResponse([
            'code' => Response::HTTP_OK,
            'data' => [
                'order' => [
                    'id'     => $order->getId(),
                    'status' => $order->getStatus(),
                ],
            ],
        ]);
    }
}
