<?php namespace Voop\Restfull\Api\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use \Voop\Restfull\Api\Service\ProductsGetService;

/**
 * Class ProductsGet
 *
 * @package Voop\Restfull\Api\Controller
 */
class ProductsGet implements ControllerInterface
{
    /**
     * @var \Voop\Restfull\Api\Service\ProductsGetService
     */
    protected $service;

    /**
     * @param \Voop\Restfull\Api\Service\ProductsGetService $service
     */
    public function __construct(ProductsGetService $service)
    {
        $this->service = $service;
    }

    /**
     *
     */
    public function handle() :JsonResponse
    {
        $products = $this->service->handle();
        return new JsonResponse([
            'code' => Response::HTTP_OK,
            'data' => json_encode(array_map(function ($product) {
                /**@var \Voop\Restfull\Api\Entity\Product $product */
                return ['id' => $product->getId(), 'title' => $product->getTitle(), 'price' => $product->getPrice()];
            }, $products))
        ]);
    }
}
