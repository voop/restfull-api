<?php namespace Voop\Restfull\Api\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use \Voop\Restfull\Api\Service\ProductsStoreService;

/**
 * Class ProductsStore
 *
 * @package Voop\Restfull\Api\Controller
 */
class ProductsStore implements ControllerInterface
{
    /** @var \Voop\Restfull\Api\Service\ProductsStoreService $service */
    protected $service;

    /**
     * @param \Voop\Restfull\Api\Service\ProductsStoreService $service
     */
    public function __construct(ProductsStoreService $service)
    {
        $this->service = $service;
    }

    /**
     *
     */
    public function handle() :JsonResponse
    {
        $this->service->handle(20);

        return new JsonResponse([
            'code' => Response::HTTP_OK,
            //'data' => []
        ]);
    }
}
