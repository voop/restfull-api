<?php namespace Voop\Restfull\Api\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Voop\Restfull\Api\Service\OrderCreateService;

/**
 * Class OrderCreate
 *
 * @package Voop\Restfull\Api\Controller
 */
class OrderCreate implements ControllerInterface
{
    /**
     * @var \Voop\Restfull\Api\Service\OrderCreateService
     */
    private $service;


    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    private $request;


    /**
     * @param \Symfony\Component\HttpFoundation\Request     $request
     * @param \Voop\Restfull\Api\Service\OrderCreateService $service
     */
    public function __construct(Request $request, OrderCreateService $service)
    {
        $this->service = $service;
        $this->request = $request;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function handle() :JsonResponse
    {
        /** @var \Voop\Restfull\Api\Entity\Order $order */
        $order = $this->service->handle($this->request->get('product_ids'));

        return new JsonResponse([
            'code' => Response::HTTP_OK,
            'data' => [
                'order' => [
                    'id'     => $order->getId(),
                    'status' => $order->getStatus(),
                ],
            ],
        ]);
    }
}
