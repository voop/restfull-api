<?php namespace Voop\Restfull\Api\Database;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

/**
 * Конфиг доктрины
 *
 * Class EntityManagerPass
 *
 * @package Voop\Restfull\Api\Database
 */
class EntityManagerPass
{
    /**
     * @param array $config
     *
     * @return EntityManager
     */
    public function configure(array $config): EntityManager
    {
        $annotationConfiguration = Setup::createAnnotationMetadataConfiguration(
            $config['doctrine']['entity_paths'],
            $config['doctrine']['dev_mode'],
            null,
            null,
            false
        );

        return EntityManager::create(
            $config['db'],
            $annotationConfiguration
        );
    }
}
