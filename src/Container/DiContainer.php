<?php namespace Voop\Restfull\Api\Container;

use Curl\Curl;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\RouteCollection;
use Voop\Restfull\Api\Core\Config;
use Voop\Restfull\Api\Core\RouteCollectionBuilder;
use Voop\Restfull\Api\Core\UrlParts;
use Voop\Restfull\Api\Database\EntityManagerPass;
use Voop\Restfull\Api\Libs\Call\HttpCall;
use Voop\Restfull\Api\Libs\Call\HttpResponce;

/**
 * Class DiContainer
 */
class DiContainer
{
    /**
     * Регистрация общих классов для DI
     *
     * @param \League\Container\Container $di
     */
    public function handle(\League\Container\Container $di)
    {
        $di->add(Config::class);
        $di->add(RouteCollection::class);
        $di->add(RouteCollectionBuilder::class)->addArguments([RouteCollection::class, Config::class]);
        $entityManagerPass = new EntityManagerPass();
        $di->add(EntityManager::class, $entityManagerPass->configure($di->get(Config::class)->get('database')));

        $di->add(HttpCall::class)->addArguments([Curl::class, HttpResponce::class]);
        $di->add(Curl::class);
        $di->add(HttpResponce::class);
    }
}
