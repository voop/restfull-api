<?php namespace Voop\Restfull\Api\Container\Controller\Interfaces;

use League\Container\Container;
use Symfony\Component\HttpFoundation\Request;

/**
 * Interface ControllerDiInterface
 *
 * @package Voop\Restfull\Api\Container\Controller\Interfaces
 */
interface ControllerDiInterface
{
    /**
     * @param \League\Container\Container               $container
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function __construct(Container $container, Request $request);
}
