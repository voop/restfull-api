<?php namespace Voop\Restfull\Api\Container\Controller;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Voop\Restfull\Api\Container\Controller\Abstracts\BaseDIController;
use Voop\Restfull\Api\Container\Controller\Interfaces\ControllerDiInterface;
use Voop\Restfull\Api\Controller\OrderCreate;
use Voop\Restfull\Api\Service\OrderCreateService;

class OrderDi extends BaseDIController implements ControllerDiInterface
{
    /**
     * @see \Voop\Restfull\Api\Controller\OrderCreate::handle()
     */
    public function handle()
    {
        $this->di->add(OrderCreate::class)->addArguments([Request::class, OrderCreateService::class]);
        $this->di->add(OrderCreateService::class)->addArgument($this->di->get(EntityManager::class));
        $this->di->add(OrderCreateService::class);
    }
}
