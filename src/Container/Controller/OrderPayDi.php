<?php namespace Voop\Restfull\Api\Container\Controller;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Voop\Restfull\Api\Controller\OrderPay;
use Voop\Restfull\Api\Providers\Yandex\YandexPayProvider;
use Voop\Restfull\Api\Service\OrderPayService;
use Voop\Restfull\Api\Container\Controller\Abstracts\BaseDIController;
use Voop\Restfull\Api\Container\Controller\Interfaces\ControllerDiInterface;
use \Voop\Restfull\Api\Libs\Call\HttpCall;

class OrderPayDi extends BaseDIController implements ControllerDiInterface
{
    /**
     * @see \Voop\Restfull\Api\Controller\OrderPay::handle()
     */
    public function handle()
    {
        // Зарегить зависимости
        $this->di->add(OrderPay::class)->addArguments([Request::class, OrderPayService::class]);
        $this->di->add(OrderPayService::class)->addArguments([EntityManager::class, YandexPayProvider::class]);
        $this->di->add(YandexPayProvider::class)->addArgument(HttpCall::class);
    }
}
