<?php namespace Voop\Restfull\Api\Container\Controller\Abstracts;

use League\Container\Container;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class BaseDIController
 *
 * @package Voop\Restfull\Api\Container\Controller
 */
abstract class BaseDIController
{
    /**
     * @var \League\Container\Container
     */
    protected $di;

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    protected $request;

    /**
     * @param \League\Container\Container               $container
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function __construct(Container $container, Request $request)
    {
        $this->di = $container;
    }
}
