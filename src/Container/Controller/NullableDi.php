<?php

namespace Voop\Restfull\Api\Container\Controller;

use Voop\Restfull\Api\Container\Controller\Abstracts\BaseDIController;
use Voop\Restfull\Api\Container\Controller\Interfaces\ControllerDiInterface;

/**
 * Class NullableDi
 *
 * @package Voop\Restfull\Api\Container\Controller
 */
class NullableDi extends BaseDIController implements ControllerDiInterface
{
    /**
     *
     */
    public function handle()
    {
        // Пустая заглушка не класть сюда ничего
    }
}
