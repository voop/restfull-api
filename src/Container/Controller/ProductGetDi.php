<?php namespace Voop\Restfull\Api\Container\Controller;

use Doctrine\ORM\EntityManager;
use Voop\Restfull\Api\Controller\ProductsGet;
use Voop\Restfull\Api\Service\ProductsGetService;
use Voop\Restfull\Api\Container\Controller\Abstracts\BaseDIController;
use Voop\Restfull\Api\Container\Controller\Interfaces\ControllerDiInterface;

/**
 * Class ProductGetDi
 *
 * @package Voop\Restfull\Api\Container\Controller
 */
class ProductGetDi extends BaseDIController implements ControllerDiInterface
{
    /**
     * @see \Voop\Restfull\Api\Controller\ProductsGet::handle()
     */
    public function handle()
    {
        $this->di->add(ProductsGet::class)->addArgument(ProductsGetService::class);
        $this->di->add(ProductsGetService::class)->addArgument($this->di->get(EntityManager::class));
    }
}
