<?php namespace Voop\Restfull\Api\Container\Controller;

use Doctrine\ORM\EntityManager;
use Voop\Restfull\Api\Controller\ProductsStore;
use Voop\Restfull\Api\Service\ProductsStoreService;
use Voop\Restfull\Api\Container\Controller\Abstracts\BaseDIController;
use Voop\Restfull\Api\Container\Controller\Interfaces\ControllerDiInterface;

/**
 * Class ProductGetDi
 *
 * @package Voop\Restfull\Api\Container\Controller
 */
class ProductsStoreDi extends BaseDIController implements ControllerDiInterface
{
    /**
     * @see \Voop\Restfull\Api\Controller\ProductsStore::store()
     */
    public function handle()
    {
        $this->di->add(ProductsStore::class)->addArgument(ProductsStoreService::class);
        $this->di->add(ProductsStoreService::class)->addArgument($this->di->get(EntityManager::class));
    }
}
