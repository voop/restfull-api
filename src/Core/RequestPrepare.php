<?php namespace Voop\Restfull\Api\Core;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class RequestPrepare
 *
 * @package Voop\Restfull\Api\Core
 */
class RequestPrepare
{
    /**
     * @return \Symfony\Component\HttpFoundation\Request
     */
    public function handle() :Request
    {
        $request = Request::createFromGlobals();
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : array());
        }

        return $request;
    }
}
