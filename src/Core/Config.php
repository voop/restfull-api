<?php namespace Voop\Restfull\Api\Core;

/**
 * Class Config
 *
 * @package Voop\Restfull\Api\Core
 */
class Config
{
    /**
     * @var array
     */
    private $conf = [];

    /**
     * @param string $name
     * @return mixed
     */
    public function get(string $name)
    {
        if (!isset($this->conf[$name])) {
            $this->conf[$name] = $this->_load($name);
        }

        return $this->conf[$name];
    }

    /**
     * @param string $name
     * @return mixed
     */
    private function _load(string $name)
    {
        return require sprintf('%s/Config/%s.php', SRC_DIR, $name);
    }
}
