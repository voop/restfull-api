<?php namespace Voop\Restfull\Api\Core;

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Преобразование из конфига в Symfony RouteCollection
 *
 * Class RouteCollectionBuilder
 *
 * @package Voop\Restfull\Api\Core
 */
class RouteCollectionBuilder
{
    /**
     * @var \Symfony\Component\Routing\RouteCollection
     */
    private static $collection;

    private static $trigg;


    /**
     * @var \Voop\Restfull\Api\Core\Config
     */
    private $config;


    /**
     * @param \Symfony\Component\Routing\RouteCollection $collection
     * @param \Voop\Restfull\Api\Core\Config             $config
     */
    public function __construct(RouteCollection $collection, Config $config)
    {
        self::$collection = $collection;
        $this->config = $config;
    }


    /**
     * @return \Voop\Restfull\Api\Core\RouteCollectionBuilder
     */
    public function handle()
    {
        if (is_null(self::$trigg)) {
            $routes = $this->config->get('routes');
            foreach ($routes as $url => $routeRules) {
                foreach ($routeRules as $rule) {
                    /** @var string $method */
                    $method = $rule['method'];
                    /** @var Route $route */
                    $route = new Route($url, $rule);
                    self::$collection->add(sprintf('%s::%s', $method, $url), $route->setMethods($method));
                }
            }

            self::$trigg = true;
        }

        return $this;
    }


    /**
     * @return \Symfony\Component\Routing\RouteCollection
     */
    public function getCollection() :RouteCollection
    {
        return self::$collection;
    }
}
