<?php namespace Voop\Restfull\Api\Core;

use League\Container\Container;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;

/**
 * Class Core
 *
 * @package Voop\Restfull\Api\Core
 */
class Core implements HttpKernelInterface
{
    /**
     * @var array
     */
    protected $routes = [];

    /**
     * @var \Voop\Restfull\Api\Core\Config
     */
    protected $config;

    /**
     * @var \League\Container\Container
     */
    protected $di;


    protected $routeAttributes;


    /**
     * @param \League\Container\Container    $di
     * @param \Voop\Restfull\Api\Core\Config $config
     */
    public function __construct(Container $di, Config $config)
    {
        $this->di = $di;
        $this->config = $config;
    }


    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param int                                       $type
     * @param bool                                      $catch
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, $type = HttpKernelInterface::MASTER_REQUEST, $catch = true)
    {
        /**
         * --------------------------------------------
         * Routes
         */
        /** @var \Voop\Restfull\Api\Core\RouteCollectionBuilder $builder */
        $builder = $this->di->get(RouteCollectionBuilder::class);
        $routes = $builder->handle()->getCollection();
        $this->routes = $routes;
        $context = new RequestContext();
        $context->fromRequest($request);
        $matcher = new UrlMatcher($routes, $context);

        try {
            $attributes = $matcher->match($request->getPathInfo());
            $this->routeAttributes = $attributes;
            $controller = $attributes['invoke'];
        } catch (ResourceNotFoundException $e) {
            return new JsonResponse('Not found!', Response::HTTP_NOT_FOUND);
        }

        try {
            /** @var \Voop\Restfull\Api\Container\Controller\Interfaces\ControllerDiInterface $controllerDi */
            $diInvoke = $attributes['di'] ?: \Voop\Restfull\Api\Container\Controller\NullableDi::class;
            (new $diInvoke($this->di, $request))->handle();
            $controller = $this->di->get($controller);

            $response = $controller->handle();
        } catch (\Exception $e) {
            return new JsonResponse($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }
}
