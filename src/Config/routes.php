<?php

// доступные роуты
return [
    // --------------------------------------
    // Order
    'api/v1/order'       => [
        [
            'method'  => 'put',
            'invoke'  => \Voop\Restfull\Api\Controller\OrderCreate::class,
            'di'      => \Voop\Restfull\Api\Container\Controller\OrderDi::class,
            'comment' => 'Создать заказ (со списком товаров)',
        ],
    ],

    // --------------------------------------
    // ProductList
    'api/v1/productlist' => [
        [
            'method'  => 'get',
            'invoke'  => \Voop\Restfull\Api\Controller\ProductsGet::class,
            'di'      => \Voop\Restfull\Api\Container\Controller\ProductGetDi::class,
            'comment' => 'Получить список товаров',
        ],
        [
            'method'  => 'put',
            'invoke'  => \Voop\Restfull\Api\Controller\ProductsStore::class,
            'di'      => \Voop\Restfull\Api\Container\Controller\ProductsStoreDi::class,
            'comment' => 'Создать N товаров',
        ],
    ],

    // --------------------------------------
    // OrderPayService
    'api/v1/orderpay'  => [
        [
            'method'  => 'post',
            'invoke'  => \Voop\Restfull\Api\Controller\OrderPay::class,
            'di'      => \Voop\Restfull\Api\Container\Controller\OrderPayDi::class,
            'comment' => 'Оплата заказа',
        ],
    ],
];
