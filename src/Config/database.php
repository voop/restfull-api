<?php

return [
    'db'       => [
        'driver'   => 'pdo_mysql',
        'host'     => 'localhost',
        'user'     => 'root',
        'password' => '123',
        'dbname'   => 'test_db',
    ],
    'doctrine' => [
        'dev_mode'     => true,
        'entity_paths' => [SRC_DIR . '/Entity'],
    ],
];
