<?php namespace Voop\Restfull\Api\Service;

use Doctrine\ORM\EntityManager;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Exception\LogicException;
use Voop\Restfull\Api\Entity\Order;
use Voop\Restfull\Api\Entity\Product;
use Voop\Restfull\Api\Service\Interfaces\ServiceInterface;

class OrderCreateService implements ServiceInterface
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param array $ids
     *
     * @return Order
     */
    public function handle(array $ids): Order
    {
        $products = $this->entityManager
            ->getRepository(Product::class)
            ->findBy(['id' => $ids]);

        if (empty($products)) {
            throw new LogicException('Products not found');
        }

        $order = new Order(
            Uuid::uuid4()->toString(),
            Order::STATUS_NEW,
            $products
        );

        $this->entityManager->persist($order);
        $this->entityManager->flush();

        return $order;
    }
}
