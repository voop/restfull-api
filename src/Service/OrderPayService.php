<?php namespace Voop\Restfull\Api\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Exception\LogicException;
use Voop\Restfull\Api\Entity\Order;
use Voop\Restfull\Api\Providers\Yandex\YandexPayProvider;
use Voop\Restfull\Api\Service\Interfaces\ServiceInterface;

/**
 * Class OrderPayService
 *
 * @package Voop\Restfull\Api\Service
 */
class OrderPayService implements ServiceInterface
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var \Voop\Restfull\Api\Providers\Yandex\YandexPayProvider
     */
    private $provider;

    /**
     * @param EntityManager     $entityManager
     * @param YandexPayProvider $provider
     */
    public function __construct(EntityManager $entityManager, YandexPayProvider $provider)
    {
        $this->entityManager = $entityManager;
        $this->provider      = $provider;
    }

    /**
     * @param string $id
     * @param int    $summ
     * @return Order
     */
    public function handle(string $id, int $summ): Order
    {
        /** @var Order $order */
        $order = $this->entityManager->getRepository(Order::class)->findOneBy(['id' => $id]);

        if (empty($order)) {
            throw new LogicException('Order not found');
        }

        $this->checkPayable($order, $summ);

        $responce = $this->provider->call();

        if ($responce->isSuccess()) {
            $order->setPaidStatus();
            $this->entityManager->persist($order);
            $this->entityManager->flush();
        }

        return $order;
    }


    /**
     * @param \Voop\Restfull\Api\Entity\Order $order
     * @param int                             $sum
     */
    protected function checkPayable(Order $order, int $sum)
    {
        if ($order->getAmount() != $sum) {
            throw new LogicException('Sum not alowed');
        }

        if (! $order->isNew()) {
            throw new LogicException('Not payable status');
        }
    }
}
