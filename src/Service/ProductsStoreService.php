<?php namespace Voop\Restfull\Api\Service;

use Doctrine\ORM\EntityManager;
use Ramsey\Uuid\Uuid;

use Voop\Restfull\Api\Entity\Product;
use Voop\Restfull\Api\Service\Interfaces\ServiceInterface;

class ProductsStoreService implements ServiceInterface
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $count
     *
     * @return bool
     */
    public function handle(int $count): bool
    {
        // TODO - Транзакции?
        for ($i = 0; $i < $count; $i++) {
            $this->entityManager->persist(new Product(Uuid::uuid4()->toString(), 'title' . $i, $i * 10));
        }
        $this->entityManager->flush();

        return true;
    }
}
