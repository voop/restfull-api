<?php namespace Voop\Restfull\Api\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Exception\LogicException;
use Voop\Restfull\Api\Entity\Product;
use Voop\Restfull\Api\Service\Interfaces\ServiceInterface;

class ProductsGetService implements ServiceInterface
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     *
     * @return array
     */
    public function handle(): array
    {
        $products = $this->entityManager
            ->getRepository(Product::class)
            ->findAll();

        if (empty($products)) {
            throw new LogicException('Products not found');
        }

        return $products;
    }
}
