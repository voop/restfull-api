<?php namespace Voop\Restfull\Api\Providers\Yandex;

use Voop\Restfull\Api\Libs\Call\CallInterface;
use Voop\Restfull\Api\Libs\Call\ResponceInterface;
use Voop\Restfull\Api\Providers\Interfaces\ProviderInterface;

/**
 * Class YandexPayProvider
 *
 * @package Voop\Restfull\Api\Providers\Yandex
 */
class YandexPayProvider implements ProviderInterface
{
    /**
     * @var \Voop\Restfull\Api\Libs\Call\CallInterface
     */
    private $callProcessor;

    /**
     * @param \Voop\Restfull\Api\Libs\Call\CallInterface $callProcessor - через что будем слать
     */
    public function __construct(CallInterface $callProcessor)
    {
        $this->callProcessor = $callProcessor;
    }

    /**
     * Куда стучаться
     * @return string
     */
    public function getSource(): string
    {
        return 'ya.ru';
    }

    /**
     *
     */
    public function call() :ResponceInterface
    {
        return $this->callProcessor->call($this)->getResult();
    }
}
