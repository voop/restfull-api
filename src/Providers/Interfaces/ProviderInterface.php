<?php namespace Voop\Restfull\Api\Providers\Interfaces;

use Voop\Restfull\Api\Libs\Call\ResponceInterface;

/**
 * Interface ProviderInterface
 *
 * @package Voop\Restfull\Api\Providers\Interfaces
 */
interface ProviderInterface
{
    /**
     * @return string
     */
    public function getSource() :string;

    /**
     * @return \Voop\Restfull\Api\Libs\Call\ResponceInterface
     */
    public function call() :ResponceInterface;
}
