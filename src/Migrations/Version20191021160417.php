<?php

declare(strict_types=1);

namespace Voop\Restfull\Api\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191021160417 extends AbstractMigration
{
    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql(
            <<<SQL
                CREATE TABLE products (
                    id VARCHAR(36) UNIQUE NOT NULL, 
                    title TEXT NOT NULL, 
                    price INT NOT NULL, 
                    PRIMARY KEY(id)
                )
SQL
        );
    }

    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE products');
    }
}
