<?php

declare(strict_types=1);

namespace Voop\Restfull\Api\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191021160500 extends AbstractMigration
{
    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql(
            <<<SQL
                CREATE TABLE orders
                (
                    id VARCHAR(36) UNIQUE NOT NULL,
                    amount INT NOT NULL,
                    status VARCHAR(255) NOT NULL,
                    PRIMARY KEY (id)
                )
SQL
        );

        $this->addSql(
            <<<SQL
                CREATE TABLE products_orders
                (
                    product_id  VARCHAR(36) NOT NULL,
                    order_id VARCHAR(36) NOT NULL,
                    PRIMARY KEY (product_id, order_id)
                )
SQL
        );
    }

    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE products_orders');
        $this->addSql('DROP TABLE orders');
    }
}
